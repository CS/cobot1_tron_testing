#include <cooperation_msgs/PressureMsg.h>
#include <moveit_msgs/PickupAction.h>
#include <moveit_msgs/PlaceAction.h>
#include <moveit_msgs/MoveItErrorCodes.h>
#include <moveit_msgs/MoveGroupAction.h>
#include <control_msgs/FollowJointTrajectoryAction.h>
#include <gazebo_msgs/LinkStates.h>
#include <geometry_msgs/Pose.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2/LinearMath/Vector3.h>
#include <algorithm> // for std::find
#include <cmath>
#include "tron_adapter.h"

TRON_Adapter adapter;
int32_t retry_count; // set in configuration_phase

void on_pressure(const cooperation_msgs::PressureMsg::ConstPtr &msg) { 
    if (msg->pressed)
        adapter.report_now("pressed", 1, &retry_count);
}

void on_pickup_result(const moveit_msgs::PickupActionResult::ConstPtr &msg) {
    if (msg->result.error_code.val == moveit_msgs::MoveItErrorCodes::SUCCESS)
        adapter.report_now("pickup_success");
    else adapter.report_now("pickup_fail");
}

void on_place_result(const moveit_msgs::PlaceActionResult::ConstPtr &msg) {
    if (msg->result.error_code.val == moveit_msgs::MoveItErrorCodes::SUCCESS)
        adapter.report_now("place_success");
    else adapter.report_now("place_fail");
}

void on_move_result(const control_msgs::FollowJointTrajectoryActionResult::ConstPtr &msg){
    if (msg->result.error_code == control_msgs::FollowJointTrajectoryResult::SUCCESSFUL)
        adapter.report_now("move_success");
    else adapter.report_now("move_fail");
}

void on_move_goal(const moveit_msgs::MoveGroupActionGoal::ConstPtr &msg){
    int32_t is_orientation_constraint_set;
    if (msg->goal.request.path_constraints.orientation_constraints.empty())
        is_orientation_constraint_set = 0;
    else is_orientation_constraint_set = 1;
    adapter.report_now("goal", 1, &is_orientation_constraint_set);
}

void on_pickup_goal(const moveit_msgs::PickupActionGoal::ConstPtr &msg){
    int32_t is_orientation_constraint_set;
    if (msg->goal.path_constraints.orientation_constraints.empty())
        is_orientation_constraint_set = 0;
    else is_orientation_constraint_set = 1;
    adapter.report_now("goal", 1, &is_orientation_constraint_set);
}

void on_place_goal(const moveit_msgs::PlaceActionGoal::ConstPtr &msg) {
    int32_t is_orientation_constraint_set;
    if (msg->goal.path_constraints.orientation_constraints.empty())
        is_orientation_constraint_set = 0;
    else is_orientation_constraint_set = 1;
    adapter.report_now("goal", 1, &is_orientation_constraint_set);
}

double get_pos_diff(geometry_msgs::Point p1, geometry_msgs::Point p2) {
    double x_d = p1.x - p2.x;
    double y_d = p1.y - p2.y;
    double z_d = p1.z - p2.z;
    return std::sqrt(x_d*x_d + y_d*y_d + z_d*z_d);
}
// factor 100 means centimeters
int32_t get_pos_diff_int32(geometry_msgs::Point p1, geometry_msgs::Point p2, int32_t factor = 100) {
    double pos_diff = get_pos_diff(p1, p2);
    return std::round(pos_diff * 100);
}
double get_ang_diff_ignoring_yaw(geometry_msgs::Quaternion q_msg1, geometry_msgs::Quaternion q_msg2) {
    tf2::Quaternion q1, q2, q_diff;
    tf2::fromMsg(q_msg1, q1);
    tf2::fromMsg(q_msg2, q2);
    q_diff = q1 * q2.inverse();
    tf2::Matrix3x3 matrix(q_diff);
    double roll, pitch, yaw;
    matrix.getRPY(roll, pitch, yaw);
    q_diff.setRPY(roll, pitch, 0); // set yaw to zero since it doesnt matter for glass or bottle
    return q_diff.getAngleShortestPath();
}
int32_t get_ang_diff_ignoring_yaw_int32_deg(geometry_msgs::Quaternion q_msg1, geometry_msgs::Quaternion q_msg2){
    return std::round(get_ang_diff_ignoring_yaw(q_msg1, q_msg2) * 180 / M_PI);
}

const std::vector<std::string> tron_pos_vars_ordered = {
    "bottle_diff_to_start_pos",
    "bottle_diff_to_start_ang",
    "bottle_diff_to_target_pos",
    "bottle_diff_to_target_ang",
    "glass_diff_to_start_pos",
    "glass_diff_to_start_ang",
    "glass_diff_to_target_pos",
    "glass_diff_to_target_ang"
};
std::vector<geometry_msgs::Pose> pose_to_compare_against; // set in configuration_phase
bool gazebo_initialized = false;
void on_gazebo_link_states(const gazebo_msgs::LinkStates::ConstPtr &msg){
    int bottle_index = -1;
    int glass_index = -1;
    for (int j = 0; j < msg->name.size(); j++) {
        if (msg->name[j] == "bottle::link") bottle_index = j;
        if (msg->name[j] == "glass::link") glass_index = j;
    }
    if (bottle_index == -1 || glass_index == -1) return;
    int32_t vars[tron_pos_vars_ordered.size()];
    vars[0] = get_pos_diff_int32(msg->pose[bottle_index].position, pose_to_compare_against[0].position);
    vars[1] = get_ang_diff_ignoring_yaw_int32_deg(msg->pose[bottle_index].orientation, pose_to_compare_against[0].orientation);
    vars[2] = get_pos_diff_int32(msg->pose[bottle_index].position, pose_to_compare_against[1].position);
    vars[3] = get_ang_diff_ignoring_yaw_int32_deg(msg->pose[bottle_index].orientation, pose_to_compare_against[1].orientation);
    vars[4] = get_pos_diff_int32(msg->pose[glass_index].position, pose_to_compare_against[2].position);
    vars[5] = get_ang_diff_ignoring_yaw_int32_deg(msg->pose[glass_index].orientation, pose_to_compare_against[2].orientation);
    vars[6] = get_pos_diff_int32(msg->pose[glass_index].position, pose_to_compare_against[3].position);
    vars[7] = get_ang_diff_ignoring_yaw_int32_deg(msg->pose[glass_index].orientation, pose_to_compare_against[3].orientation);
    for (Mapping& map : adapter.mappings) {
        if (map.topic == "/gazebo/link_states" && !map.channel.is_input) {
            for (int i = 0; i < tron_pos_vars_ordered.size(); i++) {
                if (tron_pos_vars_ordered[i] == map.channel.vars[0]) {
                    if (gazebo_initialized) {
                        adapter.report_now(map.channel.name, 1, &vars[i]);
                    }      
                }
            }
            
        }
    }
    if (!gazebo_initialized) {
        int init_diff = 1; // position difference needed to assume gazebo is initialized
        if (vars[0] > init_diff) return; // bottle init
        if (vars[4] > init_diff) return; // glass init
        gazebo_initialized = true;
    }
}

//intentionally send not expected output to end TRON testing
void on_test_done(Mapping& map, int32_t *ptr){
    adapter.report_now("intentional_fail");
}

void configuration_phase(ros::NodeHandle& nh){
    /* note: for configuration phase maximum message length is 256 Bytes, 
    therefore heap allocation can be avoided most of the time in called functions */

    nh.getParam("/planning/retries", retry_count);

    geometry_msgs::Pose pose;
    // bottle start
    nh.getParam("/object/bottle/x", pose.position.x);
    nh.getParam("/object/bottle/y", pose.position.y);
    nh.getParam("/object/bottle/z", pose.position.z);
    pose.orientation.w = 1.0;
    pose_to_compare_against.push_back(pose);
    // bottle target
    nh.getParam("/object/bottle/place/x", pose.position.x);
    nh.getParam("/object/bottle/place/y", pose.position.y);
    nh.getParam("/object/bottle/place/z", pose.position.z);
    pose.orientation.w = 1.0;
    pose_to_compare_against.push_back(pose);
    // glass start
    nh.getParam("/object/glass/x", pose.position.x);
    nh.getParam("/object/glass/y", pose.position.y);
    nh.getParam("/object/glass/z", pose.position.z);
    pose.orientation.w = 1.0;
    pose_to_compare_against.push_back(pose);
    // glass target
    nh.getParam("/object/glass/place/x", pose.position.x);
    nh.getParam("/object/glass/place/y", pose.position.y);
    nh.getParam("/object/glass/place/z", pose.position.z);
    pose.orientation.w = 1.0;
    pose_to_compare_against.push_back(pose);


    // note: since we are not an actual client (or server) 
    // we need to use the high level packet::*Action* messages.
    // custom callbacks are implemented in order to not worry about header size
    Mapping map = adapter.createMapping("/pressure", "pressed", false);
    adapter.add_var_to_mapping(map, "init_retry_count");
    adapter.mappings.push_back(map);
    adapter.output_subscribers.push_back(nh.subscribe("/pressure", 10, on_pressure));
    
    map = adapter.createMapping("/pickup/result", "pickup_fail", false);
    adapter.mappings.push_back(map);
    map = adapter.createMapping("/pickup/result", "pickup_success", false);
    adapter.mappings.push_back(map);
    adapter.output_subscribers.push_back(nh.subscribe("/pickup/result", 10, on_pickup_result));

    map = adapter.createMapping("/place/result", "place_fail", false);
    adapter.mappings.push_back(map);
    map = adapter.createMapping("/place/result", "place_success", false);
    adapter.mappings.push_back(map);
    adapter.output_subscribers.push_back(nh.subscribe("/place/result", 10, on_place_result));

    // there is a separate controller for hand movement, this one is for arm movement
    map = adapter.createMapping("/position_joint_trajectory_controller/follow_joint_trajectory/result", "move_fail", false);
    adapter.mappings.push_back(map);
    map = adapter.createMapping("/position_joint_trajectory_controller/follow_joint_trajectory/result", "move_success", false);
    adapter.mappings.push_back(map);
    adapter.output_subscribers.push_back(nh.subscribe("/position_joint_trajectory_controller/follow_joint_trajectory/result", 10, on_move_result));

    map = adapter.createMapping("/move_group/goal", "goal", false);
    adapter.add_var_to_mapping(map, "orient_constraint_set");
    adapter.mappings.push_back(map);
    adapter.output_subscribers.push_back(nh.subscribe("/move_group/goal", 10, on_move_goal));

    map = adapter.createMapping("/place/goal", "goal", false);
    adapter.add_var_to_mapping(map, "orient_constraint_set");
    adapter.mappings.push_back(map);
    adapter.output_subscribers.push_back(nh.subscribe("/place/goal", 10, on_place_goal));

    map = adapter.createMapping("/pickup/goal", "goal", false);
    adapter.add_var_to_mapping(map, "orient_constraint_set");
    adapter.mappings.push_back(map);
    adapter.output_subscribers.push_back(nh.subscribe("/pickup/goal", 10, on_pickup_goal));

    for (std::string s : tron_pos_vars_ordered) {
        map = adapter.createMapping("/gazebo/link_states", s+"_update", false);
        adapter.add_var_to_mapping(map, s);
        adapter.mappings.push_back(map);
    }
    adapter.output_subscribers.push_back(nh.subscribe("/gazebo/link_states", 10, on_gazebo_link_states));

    // used to end testing when model is done
    map = adapter.createMapping("test_done_dummy", "test_done", true);
    map.input_callback = on_test_done;
    adapter.mappings.push_back(map);
    map = adapter.createMapping("test_done_dummy", "intentional_fail", false);
    adapter.mappings.push_back(map);

    // not obvious in documentation: local variables are not supported
    // add_var_to_channel(socketfd, "ausloesen", "lokal");
    uint64_t microseconds = 1000000; // one second
    // documentation states 2 signed integers are used for some reason
    adapter.set_time_unit_and_timeout(microseconds, 600);

    // wait till subscribers initialized
    for (ros::Publisher& pub : adapter.input_publishers) {
        while (pub.getNumSubscribers() == 0) { ros::Duration(1).sleep(); };
    }
}

int main(int argc, char**argv){
    ros::init(argc, argv, "TRON adapter");
    ros::NodeHandle nh;

    try {
        const std::string IP = "127.0.0.1";
        const uint16_t PORT = 8080;
        adapter = TRON_Adapter(IP, PORT);

        configuration_phase(nh);
        
        adapter.request_start(); 
        
        // testing phase loop
        ros::Rate test_phase_freq(10);
        while (ros::ok()) {
            ros::spinOnce();
            adapter.process_TRONs_msgs();
            test_phase_freq.sleep();
        } 
    } catch (const char* err){
        ROS_FATAL("shutting down: %s", err);
        ros::shutdown();
    } catch (...){
    	ROS_FATAL("shutting down");
    	ros::shutdown();
    }
}
