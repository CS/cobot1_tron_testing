//
// Created by jim on 04.01.21.
//

#include "Cobot.h"
#include "PickPlace.h"
#include <TrajectoryTool.h>
#include <grasp_util.h>


Cobot::Cobot(moveit::planning_interface::MoveGroupInterface &group){
    this->start_state_joint_values = group.getCurrentJointValues();
    this->orientationConstraint = OrientationConstraint();
    this->proximityConstraint = ProximityConstraint();
    this->velocityConstraint = VelocityConstraint();
    this->accelerationConstraint = AccelerationConstraint();
}


bool Cobot::moveToStart(moveit::planning_interface::MoveGroupInterface &group, bool with_constraints) {

    if(!with_constraints) {
        this->orientationConstraint.remove(group);
        this->velocityConstraint.remove(group);
        this->accelerationConstraint.remove(group);
    }

    group.setJointValueTarget(this->start_state_joint_values);
    group.plan(this->movePlan);

    return group.execute(this->movePlan) == moveit_msgs::MoveItErrorCodes::SUCCESS;
}


bool Cobot::pickBottle(moveit::planning_interface::MoveGroupInterface &group, moveit_msgs::CollisionObject bottle, int retry_on_failure_count, double retry_planning_time_factor) {

    ROS_INFO("\n-----------------------\nPicking up the bottle...\n-----------------------");

    int tries = 0;
    bool success = false;
   // GraspUtil grasp_util;

    while(!success && tries < retry_on_failure_count){

      /*  double open_amount = bottle.primitives[0].dimensions[0] < bottle.primitives[0].dimensions[1] ? bottle.primitives[0].dimensions[0] : bottle.primitives[0].dimensions[1];
        geometry_msgs::Vector3 dimensions;
        dimensions.x = bottle.primitives[0].dimensions[0];
        dimensions.y = bottle.primitives[0].dimensions[1];
        dimensions.z = bottle.primitives[0].dimensions[2];

        success = grasp_util.pickObjectOnRobotfrontFromSide(group, bottle.id, bottle.pose, open_amount, dimensions, 1);*/

        success = PickPlace::pick_x(group, bottle) == moveit_msgs::MoveItErrorCodes::SUCCESS;
        if(!success) {
            ROS_ERROR("[CC][Cobot1] Retry bottle pick");
        }
        tries++;
    }

    // hold bottle upright
    this->orientationConstraint.apply(group);

    // lower acceleration to prevent spilling
    this->accelerationConstraint.apply(group);

    return success;
}

bool Cobot::fillGlass(moveit::planning_interface::MoveGroupInterface &group, moveit_msgs::CollisionObject glass, int retry_on_failure_count, double retry_planning_time_factor) {

    ROS_INFO("\n-----------------------\nFilling the glass...\n-----------------------");

    moveit::planning_interface::MoveGroupInterface::Plan plan;
    geometry_msgs::Pose start_pose;
    group.setPlanningTime(group.getPlanningTime() * retry_planning_time_factor);

    int y_direction = 1;
    double planning_time;
    double eef_rotation = INFINITY;
    double pouring_rotation_value = (M_PI * 5/8);

    while(ros::ok()) {
        // Move to pouring pose
        start_pose.position = glass.primitive_poses[0].position;
        start_pose.position.z = start_pose.position.z + glass.primitives[0].dimensions[2]/2 + 0.09; // TODO refactor magic numbers
        start_pose.position.y = start_pose.position.y + (glass.primitives[0].dimensions[1]/2 + 0.04) * y_direction; // hold next to the glass
        start_pose.position.x = start_pose.position.x - 0.1;
        start_pose.orientation = group.getCurrentPose().pose.orientation;
        group.setPoseTarget(start_pose);

        // If the planning fails retry
        if(group.plan(plan) == moveit_msgs::MoveItErrorCodes::SUCCESS) {
            // Get the end effector rotation in the goal state
            eef_rotation = plan.trajectory_.joint_trajectory.points.back().positions[6];

            if(std::abs(eef_rotation + pouring_rotation_value * y_direction) < 2.89725){
                // 2.89725 is the maximal rotation for panda_joint7 -> The filling rotation should be executable
                ROS_INFO("\n-----------------------\nMove to filling position...\n-----------------------");
                group.move();
                group.setPlanningTime(25);
                break;
            }
        }
        ROS_ERROR("[CC][Cobot1] Failed placing the bottle next to the glass");
        // Retry from the opposite side of the glass
        y_direction *= -1;
        planning_time += 5;
        group.setPlanningTime(planning_time);
    }

    this->orientationConstraint.remove(group); // Remove orientation constraints to allow rotation on eef joint
    this->velocityConstraint.apply(group); // Constrain the velocity to slowly pour the liquid

    // Get the current joint values and add a rotation to the end effector
    std::vector<double> joint_values;
    group.getCurrentState()->copyJointGroupPositions(group.getCurrentState()->getRobotModel()->getJointModelGroup(group.getName()), joint_values);
    double preFillJointValue = joint_values[6];
    joint_values[6] += pouring_rotation_value * y_direction ;
    group.setJointValueTarget(joint_values);

    // If planning fails retry
    while(group.plan(plan) != moveit_msgs::MoveItErrorCodes::SUCCESS) {
        ROS_WARN("[CC][Cobot1] Failed placing the bottle! Retrying...");
    }

    ROS_INFO("\n-----------------------\nFill the glass...\n-----------------------");
    group.move();

    // Hold a second to finish pouring
    ros::Duration(1.0).sleep();

    // rotate back
    joint_values[6] = preFillJointValue;
    group.setJointValueTarget(joint_values);

    int tries = 0;
    bool success = false;

    while(!success && tries < retry_on_failure_count){
        success = group.plan(plan) == moveit_msgs::MoveItErrorCodes::SUCCESS;
        if(!success){
            ROS_WARN("[CC][Cobot1] Retry to stop filling...");
        }
        tries++;
    }

    if(tries == retry_on_failure_count){
        ROS_ERROR("[CC][Cobot1] Glass fill planning failed.");
        return false;
    }

    ROS_INFO("\n-----------------------\nStop filling...\n-----------------------");
    group.move();

    this->accelerationConstraint.remove(group); // Since the bottle is empty now, the acceleration constraint is no longer necessary
    this->velocityConstraint.remove(group); // Remove the velocity constraint
    this->orientationConstraint.apply(group); // Reapply orientation to keep the bottle upright again

    group.setPlanningTime(group.getPlanningTime() / retry_planning_time_factor);
    return true;
}

bool Cobot::placeBottle(moveit::planning_interface::MoveGroupInterface &group, moveit_msgs::CollisionObject bottle, int retry_on_failure_count, double retry_planning_time_factor) {
    ROS_INFO("\n-----------------------\nPlace the bottle...\n-----------------------");

    // Get the bottles place position
    ros::NodeHandle nh;
    nh.getParam("/object/bottle/place/x", bottle.pose.position.x);
    nh.getParam("/object/bottle/place/y", bottle.pose.position.y);
    nh.getParam("/object/bottle/place/z", bottle.pose.position.z);

    group.setGoalPositionTolerance(0.05);
    group.setPlanningTime(group.getPlanningTime() * retry_planning_time_factor);

    int tries = 0;
    bool success = false;

    while(!success && tries < retry_on_failure_count){
        // Place the bottle on its original position
        success = PickPlace::place(group, bottle, bottle.pose.position) == moveit_msgs::MoveItErrorCodes::SUCCESS;
        if(!success){
            ROS_ERROR("[CC][Cobot1] Retry bottle ṕlace");
        }
        tries++;
    }

    this->orientationConstraint.remove(group); // After placing the bottle constraining the orientation is unnecessary
    group.setGoalPositionTolerance(0.01);
    group.setPlanningTime(group.getPlanningTime() / retry_planning_time_factor);
    return success;
}

bool Cobot::pickGlass(moveit::planning_interface::MoveGroupInterface &group, moveit_msgs::CollisionObject glass, int retry_on_failure_count, double retry_planning_time_factor) {

    ROS_INFO("\n-----------------------\nPick up the glass...\n-----------------------");
    group.setPlanningTime(group.getPlanningTime() * retry_planning_time_factor);

    // Try to pick up the glass

    int tries = 0;
    bool success = false;

    while(!success && tries < retry_on_failure_count){
        // Place the bottle on its original position
        success = PickPlace::pick_z(group, glass) == moveit_msgs::MoveItErrorCodes::SUCCESS;
        if(!success){
            ROS_ERROR("[CC][Cobot1] Retry bottle pick");
        }
        tries++;
    }

    this->orientationConstraint.apply(group); // Hold the glass upright
    this->accelerationConstraint.apply(group); // Constrain acceleration to prevent spilling
    this->proximityConstraint.set_min_distance(0.1);
    this->proximityConstraint.apply(group); // Keep a minimum distance of 10cm to the table

    group.setPlanningTime(group.getPlanningTime() / retry_planning_time_factor);
    return success;
}

bool Cobot::placeGlass(moveit::planning_interface::MoveGroupInterface &group, moveit_msgs::CollisionObject glass,
        geometry_msgs::Point target_place_position, int retry_on_failure_count, double retry_planning_time_factor) {
    ROS_INFO("\n-----------------------\nPlace the glass...\n-----------------------");

    this->proximityConstraint.remove(group); // Remove proximity constraint to allow the placement on the table
    this->orientationConstraint.apply(group); // Reapply orientation because removing the proximity constraint removes all path constraints
    this->accelerationConstraint.set_scaling_factor(0.05);
    this->accelerationConstraint.apply(group);

    geometry_msgs::Pose pre_place_pose;
    moveit::planning_interface::MoveGroupInterface::Plan pre_place_plan;
    group.setPlanningTime(group.getPlanningTime() * retry_planning_time_factor);

    // Move near the placing position
    pre_place_pose.position = target_place_position;
    // x: 0.106 y: 0 z: 0.3
    pre_place_pose.position.z = pre_place_pose.position.z + 0.3;
    pre_place_pose.orientation = group.getCurrentPose().pose.orientation;
    group.setPoseTarget(pre_place_pose);

    group.setGoalPositionTolerance(0.1);

    int pre_place_retries = 0;
    bool pre_place_success = false;

    while(!pre_place_success && pre_place_retries < retry_on_failure_count){
        pre_place_success = group.plan(pre_place_plan) == moveit_msgs::MoveItErrorCodes::SUCCESS;
        ROS_ERROR("[CC][Cobot1] Retry planning for pre place position...");
        pre_place_retries++;
    }

    if(pre_place_retries == retry_on_failure_count){
        ROS_ERROR("[CC][Cobot1] Glass pre-placement planning failed.");
        return false;
    }

    group.execute(pre_place_plan);
    group.setGoalPositionTolerance(0.02);

    ROS_ERROR("[CC][Cobot1] Place the glass...");
    int tries = 0;
    bool placeSuccess = false;

    while(tries < retry_on_failure_count && !placeSuccess){

        placeSuccess = (PickPlace::place(group, glass, target_place_position) == moveit_msgs::MoveItErrorCodes::SUCCESS);

        if(placeSuccess){
            ROS_INFO("[CC][Cobot1] Successfull planning for glass placement");
            break;
        }else{
            ROS_WARN("[CC][Cobot1] Retry planning for glass placement");
        }

        if(!placeSuccess && tries == 0){
            ROS_WARN("[CC][Cobot1] Stepping back.");
            moveit::planning_interface::MoveGroupInterface::Plan my_plan;
            auto currentPose = group.getCurrentPose();
            currentPose.pose.position.z = currentPose.pose.position.z + 0.1;
            group.setPoseTarget(currentPose);
            group.plan(my_plan);
            group.move();
        }
        tries++;
    }

    this->orientationConstraint.remove(group);
    this->accelerationConstraint.remove(group);
    group.setPlanningTime(group.getPlanningTime() / retry_planning_time_factor);

    if(tries == retry_on_failure_count){

        ROS_ERROR("[CC][Cobot1] Reverting to last state.");

        moveit::planning_interface::MoveGroupInterface::Plan my_plan;
        auto currentPose = group.getCurrentPose();
        currentPose.pose.position.z = currentPose.pose.position.z - 0.1;
        group.setPoseTarget(currentPose);
        group.execute(my_plan);

        moveit::planning_interface::MoveGroupInterface::Plan reverted_pre_place_plan;
        group.execute(TrajectoryTool::invert_plan(pre_place_plan, group, "panda_arm"));

        ROS_ERROR("[CC][Cobot1] Glass placement planning failed.");
        return false;
    }

    return true;
}



