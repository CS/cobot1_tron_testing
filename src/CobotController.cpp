//
// Created by jim on 04.01.21.
//

// ROS
#include <ros/ros.h>

// MoveIt
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit_msgs/CollisionObject.h>

// Visual tools
#include <moveit_visual_tools/moveit_visual_tools.h>

// Cooperation messages
#include <cooperation_msgs/PressureMsg.h>
#include <cooperation_msgs/SafeZone.h>

#include "Cobot.h"
#include "ObjectCreator.h"


bool pressed = false;
int RETRY_ON_FAILURE_COUNT = 5;
double RETRY_PLANNING_TIME_FACTOR = 1;

/**
 * Callback function for incoming messages on the "pressure1" topic
 * @param msg the received message
 */
void on_pressure(const cooperation_msgs::PressureMsg::ConstPtr &msg) {
    ROS_INFO_NAMED("Pressure1", "Received change in pressure 1 state");
    pressed = msg->pressed;
}

/**
 * Call the safe_zone_service and try to occupy the safe zone
 * @param safeZoneClient client that can call the safe_zone_service
 * @return whether the occupation was successful
 */
bool occupySafeZone(ros::ServiceClient &safeZoneClient) {
    cooperation_msgs::SafeZone srv;
    srv.request.occupy = true;

    if (safeZoneClient.call(srv)) {
        ROS_INFO_NAMED("Safe zone service response", "Allowed to enter the safe zone: %d", srv.response.success);
        return srv.response.success;
    } else {
        ROS_ERROR("Failed to call safe_zone_controller");
        return false;
    }
}

/**
 * Call the safe_zone_service and try to unblock the safe zone
 * @param safeZoneClient client that can call the safe_zone_service
 * @return whether the unblock was successful
 */
bool freeSafeZone(ros::ServiceClient &safeZoneClient) {
    cooperation_msgs::SafeZone srv;
    srv.request.occupy = false;

    if (safeZoneClient.call(srv)) {
        ROS_INFO_NAMED("Safe zone service response", "Unblocked the safe zone: %d", srv.response.success);
        return srv.response.success;
    } else {
        ROS_ERROR("Failed to call safe_zone_controller");
        return false;
    }
}

int main(int argc, char **argv) {
    // ROS initialization
    ros::init(argc, argv, "cobot1_controller");
    ros::NodeHandle node_handle;
    ros::AsyncSpinner spinner(3);
    spinner.start();

    ros::Duration(3.0).sleep();

    moveit::planning_interface::MoveGroupInterface group("panda_arm");
    moveit::planning_interface::PlanningSceneInterface planning_scene_interface;

    group.setPlanningTime(25.0);
    group.setNumPlanningAttempts(3);
    group.setGoalTolerance(0.01);
    // group.setPlannerId("RRTstarkConfigDefault");
    // group.setPlanningPipelineId()

    // Setting up retries
    node_handle.getParam("/planning/retries", RETRY_ON_FAILURE_COUNT);
    node_handle.getParam("/planning/time_factor", RETRY_PLANNING_TIME_FACTOR);
    ROS_INFO_STREAM("[CC] Setting planning retries to " << RETRY_ON_FAILURE_COUNT);
    ROS_INFO_STREAM("[CC] Setting planning time factor to " << RETRY_PLANNING_TIME_FACTOR);

    // Subscribe to pressure sensor 1
    ros::Subscriber pressure_1_sub = node_handle.subscribe("pressure", 10, on_pressure);

    // Create the safe zone client to occupy the safe zone if needed
    ros::ServiceClient safeZoneClient = node_handle.serviceClient<cooperation_msgs::SafeZone>("safe_zone_controller");


    // Add objects to the environment
    ObjectCreator oc(planning_scene_interface);
    oc.createTable(ObjectConfig("/object/table/"), "table1");
    moveit_msgs::CollisionObject glass = oc.createBox(ObjectConfig("/object/glass/"), "glass", true);
    geometry_msgs::Point original_pick_location;
    original_pick_location.x = glass.primitive_poses[0].position.x;
    original_pick_location.y = glass.primitive_poses[0].position.y;
    original_pick_location.z = glass.primitive_poses[0].position.z;

    moveit_msgs::CollisionObject bottle = oc.createBox(ObjectConfig("/object/bottle/"), "bottle", true);
    moveit_msgs::CollisionObject pressure_sensor_1 = oc.createBox(ObjectConfig("/object/pressure_sensor1/"),
                                                                  "pressure_sensor1", true);
    moveit_msgs::CollisionObject pressure_sensor_2 = oc.createBox(ObjectConfig("/object/pressure_sensor2/"),
                                                                  "pressure_sensor2", true);

    SafeZoneConstraint safeZone(planning_scene_interface, ObjectConfig("/object/safezone/"));
    safeZone.apply(group);

    // Attach the table to the robot to allow collisions between the base link and the table
    std::vector<std::string> touch_links;
    touch_links.push_back("panda_link1");
    touch_links.push_back("panda_link0");
    group.attachObject("table1", "panda_link0", touch_links);

    // Set support surface as table1.
    group.setSupportSurfaceName("table1");

    ros::Duration(2.0).sleep();

    // Initialize the cobot
    Cobot cobot(group);

    while (ros::ok()) {

        cobot.moveToStart(group, false);

        while (!pressed) {
            // wait while no glass present
            ros::spinOnce();
            ros::Duration(0.5).sleep();
        }

        if (!cobot.pickBottle(group, bottle, RETRY_ON_FAILURE_COUNT, RETRY_PLANNING_TIME_FACTOR)) {
            ROS_ERROR("[CC] Failed picking up the bottle.");
            return 0;
        }

        if (!cobot.fillGlass(group, glass, RETRY_ON_FAILURE_COUNT, RETRY_PLANNING_TIME_FACTOR)) {
            ROS_ERROR("[CC] Failed filling the glass..Retrying");
            return 0;
        }

        if (!cobot.placeBottle(group, bottle, RETRY_ON_FAILURE_COUNT, RETRY_PLANNING_TIME_FACTOR)) {
            ROS_ERROR("[CC] Failed placing the bottle..Retrying");
            return 0;
        }

        ros::Duration(0.5).sleep();

        if (!cobot.pickGlass(group, glass, RETRY_ON_FAILURE_COUNT, RETRY_PLANNING_TIME_FACTOR)) {
            ROS_ERROR("[CC] FATAL ... Failed to pick glass.");
            return 0;
        }

        while (!occupySafeZone(safeZoneClient)) { // if not allowed to enter the safe zone, wait and try again
            ros::Duration(0.5).sleep();
        }

        safeZone.remove(group); // remove the safe zone obstacle if allowed to enter

        ros::Duration(0.5).sleep();

        // Glass will be place on the opposite site of the robot
        geometry_msgs::Point target_place_position;
        node_handle.getParam("/object/glass/place/x", target_place_position.x);
        node_handle.getParam("/object/glass/place/y", target_place_position.y);
        node_handle.getParam("/object/glass/place/z", target_place_position.z);

        int place_glass_retry = 0;
        while (!cobot.placeGlass(group, glass, target_place_position, RETRY_ON_FAILURE_COUNT, RETRY_PLANNING_TIME_FACTOR)) {

            ROS_ERROR("[CC] Falling back ... cannot place.");

            // do fallback

            if (!cobot.placeGlass(group, glass, original_pick_location, RETRY_ON_FAILURE_COUNT, RETRY_PLANNING_TIME_FACTOR)) {
                ROS_ERROR("[CC] FATAL ... Failed to do fallback.");
                return 0;

            } else {
                ROS_ERROR("[CC] Successfully executed fallback.");
                ROS_ERROR("[CC] Re-Picking the glass");

                if (!cobot.pickGlass(group, glass, RETRY_ON_FAILURE_COUNT, RETRY_PLANNING_TIME_FACTOR)) {
                    ROS_ERROR("[CC] FATAL cannot repick");
                    return 0;
                }
                ROS_INFO("[CC] Re-Pick successfull.");
            }

            ROS_ERROR("[CC] Failed placing the glass ... Retrying");
            place_glass_retry++;

            if (place_glass_retry == RETRY_ON_FAILURE_COUNT) {
                ROS_ERROR("[CC] FATAL ... Failed to place glass.");
                return 0;
            }
        }

        cobot.moveToStart(group, false);
        safeZone.apply(group); // reapply the safe zone constraint
        freeSafeZone(safeZoneClient); // Unblock the safe zone for other robots

        ROS_INFO_NAMED("FINISHED", "Completed demo execution");
        ROS_ERROR("[CC] FINISHED");
        ros::shutdown();
    }

    return 0;
}