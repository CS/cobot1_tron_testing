#!/bin/bash

start_dir=$PWD
test_count=100
ros_workspace_location="/home/cs/Schreibtisch/panda_gazebo_workspace"
package_name="cobot_1"
roslaunch_filename="cobot1_mock_test.launch"
tron_location="/home/cs/Schreibtisch/TRON/tron"
model_location="/home/cs/Schreibtisch/TRON/cobot1.xml"

# make sure package was built and devel/setup.bash was run before starting
cd $ros_workspace_location
catkin build $package_name
source devel/setup.bash

tron_args=( # do not set -o here
    "-I SocketAdapter" 
    "-v 10"
    "-q"
    $model_location # needs to be specified before port
    "-- 8080"
)

# tron_args to one string
tron_args_str=${tron_args[0]}
for single_arg in ${tron_args[@]:1}; do
    tron_args_str+=" "
    tron_args_str+=$single_arg
done

echo "args given to TRON: "
echo $tron_args_str

for ((i=1;i<=test_count;i++)); do 
    tron_args_str_output="-o $start_dir/test_output$i $tron_args_str"
    $tron_location $tron_args_str_output & tron_pid=$!
    roslaunch $package_name $roslaunch_filename & roslaunch_pid=$!
    trap "kill $tron_pid; kill $roslaunch_pid; wait $tron_pid; wait $roslaunch_pid; cd $start_dir; return 1" SIGINT # kill both on interrupt
    wait $tron_pid
    kill $roslaunch_pid
    wait $roslaunch_pid # wait till actually killed, takes few seconds
    echo "test $i done"
done

cd $start_dir
return 0
