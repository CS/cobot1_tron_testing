#!/bin/bash

fn="test_output"
max_test_num=100
found_succ=0
found_shut_down=0
found_fail=0
success_nums=()
shut_down_nums=()
fail_nums=()

for ((i=1;i<=$max_test_num;i++)); do
	found=false
	echo "reading file $i"
	while read line; do
	
		if [[ $line == *"Cobot.finished_success"* ]]; then
		  found=true
		  ((found_succ++))
		  success_nums+=($i)
		  break
		fi
		
		if [[ $line == *"Cobot.shut_down"* ]]; then
		  found=true
		  ((found_shut_down++))
		  shut_down_nums+=($i)
		  break
		fi
		
	done < "$fn$i"
	
	if ! $found; then
		((found_fail++))
		fail_nums+=($i)
	fi
done

echo "success: $found_succ"
nums_str=""
for i in ${success_nums[@]}; do
nums_str="$nums_str $i"
done
echo "success_nums:$nums_str"

echo "shut_down: $found_shut_down"
nums_str=""
for i in ${shut_down_nums[@]}; do
nums_str="$nums_str $i"
done
echo "shut_down_nums:$nums_str"

nums_str=""
echo "fails: $found_fail"
for i in ${fail_nums[@]}; do
nums_str="$nums_str $i"
done
echo "fail_nums:$nums_str"
	
