#!/bin/bash
for i in {1..25}
do
   echo "LAUNCH FILE LOOP $i"
   roslaunch cobot_1 cobot1_minimal_mock.launch > log-run-$i.txt 2>&1
done
