//
// Created by jim on 04.01.21.
//

#ifndef COBOT_1_COBOT_H
#define COBOT_1_COBOT_H

#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <joint_limits_interface/joint_limits_rosparam.h>
#include "Constraint.h"



class Cobot {
public:
    // ctor
    Cobot(moveit::planning_interface::MoveGroupInterface &group);

    // member functions

    /**
     * Move the robot to its start position
     * @param group the move_group on which the action shall be performed
     */
    virtual bool moveToStart(moveit::planning_interface::MoveGroupInterface &group, bool with_constraints);

    /**
     * Perform a grasping action on the bottle
     * @param group  the move_group on which the action shall be performed
     * @param bottle the collision object that represents the bottle
     * @return true if the execution was successful, else false
     */
    virtual bool pickBottle(moveit::planning_interface::MoveGroupInterface &group, moveit_msgs::CollisionObject bottle,
            int retry_on_failure_count = 1, double retry_planning_time_factor = 1);

    /**
     * Fill the glass by moving the bottle closer to it and rotate the end effector to pour the liquid
     * @param group the move_group on which the action shall be performed
     * @param glass the glass to be filled
     * @return true if the execution was successful, else false
     */
    virtual bool fillGlass(moveit::planning_interface::MoveGroupInterface &group, moveit_msgs::CollisionObject glass,
            int retry_on_failure_count = 1, double retry_planning_time_factor = 1);

    /**
     * Place the bottle back on its start position
     * @param group the move_group on which the action shall be performed
     * @param bottle the collision object that represents the bottle
     * @return true if the execution was successful, else false
     */
    virtual bool placeBottle(moveit::planning_interface::MoveGroupInterface &group, moveit_msgs::CollisionObject bottle,
            int retry_on_failure_count = 1, double retry_planning_time_factor = 1);

    /**
     * Pick up the glass
     * @param group the move_group on which the action shall be performed
     * @param glass the collision object that represents the glass
     * @return true if the execution was successful, else false
     */
    virtual bool pickGlass(moveit::planning_interface::MoveGroupInterface &group, moveit_msgs::CollisionObject glass,
            int retry_on_failure_count = 1, double retry_planning_time_factor = 1);

    /**
     * Place the glass on the position defined in the glass object
     * @param group group the move_group on which the action shall be performed
     * @param glass the collision object that represents the glass and holds the goal position
     * @return true if the execution was successful, else false
     */
    virtual bool placeGlass(moveit::planning_interface::MoveGroupInterface &group, moveit_msgs::CollisionObject glass,
            geometry_msgs::Point target_place_position, int retry_on_failure_count = 1, double retry_planning_time_factor = 1);

private:
    std::vector<double> start_state_joint_values;
    moveit::planning_interface::MoveGroupInterface::Plan movePlan;
    ProximityConstraint proximityConstraint;
    OrientationConstraint orientationConstraint;
    VelocityConstraint velocityConstraint;
    AccelerationConstraint accelerationConstraint;
};


#endif //COBOT_1_COBOT_H
